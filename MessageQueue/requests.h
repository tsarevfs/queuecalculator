#ifndef REQUESTS_H
#define REQUESTS_H

enum MessageQueueRequests
{
    MQ_PUT = 0,
    MQ_GET = 1,
    MQ_SUBSCRIBE = 2
};

enum ResponseStatus
{
    RS_OK = 0,
    RS_DENIED = 1
};

#endif // REQUESTS_H
