#-------------------------------------------------
#
# Project created by QtCreator 2014-04-25T16:08:08
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = MessageQueue
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    messagequeue.cpp

HEADERS += \
    messagequeue.h \
    serialisation.h \
    requests.h \
    exceptions.h
QMAKE_CXXFLAGS += -std=c++0x
