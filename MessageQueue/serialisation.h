#ifndef COMMON_H
#define COMMON_H

#include <QtEndian>
#include <QByteArray>

inline QByteArray serializedInt(qint32 value)
{
    QByteArray buffer;
    buffer.resize(sizeof(qint32));
    qToBigEndian<qint32>(value, reinterpret_cast<unsigned char *>(buffer.data()));
    return buffer;
}

inline qint32 deserealizedInt(QByteArray buffer)
{
    unsigned char * buff = (unsigned char *)buffer.data();
    return qFromBigEndian<qint32>(buff);
}


#endif // COMMON_H
