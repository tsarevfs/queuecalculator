#ifndef MESSAGEQUEUE_H
#define MESSAGEQUEUE_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QHostAddress>
#include <QUdpSocket>
#include <QByteArray>
#include <QQueue>
#include <QMap>
#include <QPair>
#include <QList>
#include <memory>

class MessageQueue : QObject
{
    Q_OBJECT
public:
    explicit MessageQueue(QObject *parent = 0);

protected:
    void run();
    
signals:
    
public slots:
    void newConnection();
    void clientReadReady();
    void clientDisconnected();

private:
    void processInputRequestData(int descr, QHostAddress address);
    void put(QString queueName, QByteArray data);
    void responseGet(int descr, QString queueName);
private:
    QTcpServer *tcpServer_;
    QUdpSocket *udpSocket_;

    int tcpListenPort_;
    int udpClientPort_;
    QMap<int, std::shared_ptr<QTcpSocket> > inputRequestSockets_;
    QMap<int, QByteArray> inputRequestBuffer_;

    QMap< QString, QList< QPair< QHostAddress, int> > > subscribers_;

    QMap<QString, QQueue<QByteArray> > messageQueues_;
    
};

#endif // MESSAGEQUEUE_H
