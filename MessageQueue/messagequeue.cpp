#include "messagequeue.h"
#include "serialisation.h"
#include "requests.h"
#include "exceptions.h"

MessageQueue::MessageQueue(QObject *parent) :
    QObject(parent)
  , tcpServer_(new QTcpServer(this))
  , udpSocket_(new QUdpSocket(this))
  , tcpListenPort_(55555)
  , udpClientPort_(55556)
{
    qDebug() << "queue started";
    connect(tcpServer_, SIGNAL(newConnection()), this, SLOT(newConnection()));

    if (!tcpServer_->listen(QHostAddress::Any, tcpListenPort_))
    {
        qDebug() <<tcpServer_->errorString();
        throw SocketError(tcpServer_->errorString().toStdString());
    }
    udpSocket_->bind(55559);
}

void MessageQueue::newConnection()
{
    qDebug() << "new connection";

    while(tcpServer_->hasPendingConnections())
    {
        QTcpSocket *inputRequestSocket = tcpServer_->nextPendingConnection();
        connect(inputRequestSocket, SIGNAL(readyRead()),    this, SLOT(clientReadReady()));
        connect(inputRequestSocket, SIGNAL(disconnected()),    this, SLOT(clientDisconnected()));


        int descr = inputRequestSocket->socketDescriptor();
        inputRequestSockets_[descr].reset(inputRequestSocket);
    }
}

void MessageQueue::clientReadReady()
{
    QTcpSocket* inputRequestSocket = (QTcpSocket*)sender();
    int descr = inputRequestSocket->socketDescriptor();

    inputRequestBuffer_[descr].append(inputRequestSocket->readAll());

    qDebug() << "incomming request";
    processInputRequestData(descr, inputRequestSocket->peerAddress());
}

void MessageQueue::clientDisconnected()
{
    QTcpSocket* inputRequestSocket = (QTcpSocket*)sender();
    int descr = inputRequestSocket->socketDescriptor();

    inputRequestSockets_.remove(descr);
}

void MessageQueue::processInputRequestData(int descr, QHostAddress address)
{
    QByteArray buffer = inputRequestBuffer_[descr];

    while (buffer.size() >= 1)
    {

        char requestCode = buffer[0];
        int pos = 1;

        if (requestCode == MQ_PUT)
        {
            if (buffer.size() < pos + (int)sizeof(qint32))
                return;

            int queueNameLen = deserealizedInt(buffer.mid(pos, sizeof(qint32)));
            pos += sizeof(qint32);

            if (buffer.size() < pos + queueNameLen + (int)sizeof(qint32))
                return;

            QString queueName = QString::fromUtf8(buffer.mid(pos, queueNameLen));
            pos += queueNameLen;

            int dataLen = deserealizedInt(buffer.mid(pos, sizeof(qint32)));
            pos += sizeof(qint32);

            if (buffer.size() < pos + dataLen)
                return;

            QByteArray data = buffer.mid(pos, dataLen);
            pos += dataLen;


            put(queueName, data);
        }
        else if (requestCode == MQ_GET)
        {
            if (buffer.size() < pos + (int)sizeof(qint32))
                return;

            int queueNameLen = deserealizedInt(buffer.mid(pos, sizeof(qint32)));
            pos += sizeof(qint32);

            if (buffer.size() < pos + queueNameLen)
                return;

            QString queueName = QString::fromUtf8(buffer.mid(pos, queueNameLen));
            pos += queueNameLen;

            responseGet(descr, queueName);
        }
        else if (requestCode == MQ_SUBSCRIBE)
        {
            if (buffer.size() < pos + 2 * (int)sizeof(qint32))
                return;

            int udpPort = deserealizedInt(buffer.mid(pos, sizeof(qint32)));
            pos += sizeof(qint32);

            int queueNameLen = deserealizedInt(buffer.mid(pos, sizeof(qint32)));
            pos += sizeof(qint32);

            if (buffer.size() < pos + queueNameLen)
                return;

            QString queueName = QString::fromUtf8(buffer.mid(pos, queueNameLen));
            pos += queueNameLen;

            auto clientAddresPort = qMakePair(address, udpPort);
            qDebug() << address.toString();
            if (!subscribers_[queueName].contains(clientAddresPort))
                subscribers_[queueName].append(clientAddresPort);
        }

        buffer = buffer.mid(pos);
    }
    inputRequestBuffer_[descr] = buffer;
}

void MessageQueue::put(QString queueName, QByteArray data)
{
    messageQueues_[queueName].append(data);
    foreach(auto subscriber, subscribers_[queueName])
    {
        QByteArray notify = queueName.toUtf8();
        int sended = udpSocket_->writeDatagram(notify, subscriber.first, subscriber.second);
        if (sended > 0)
            qDebug() << "notification sended";
        else
            throw SocketError(udpSocket_->errorString().toStdString());
    }
}

void MessageQueue::responseGet(int descr, QString queueName)
{
    QByteArray response;

    if (!messageQueues_.contains(queueName) || messageQueues_[queueName].empty())
    {
        response.append((char)RS_DENIED);
    }
    else
    {
        QByteArray data = messageQueues_[queueName].front();
        messageQueues_[queueName].removeFirst();

        response.append((char)RS_OK);
        response.append(serializedInt(data.size()));
        response.append(data);
    }

    inputRequestSockets_[descr]->write(response);

    if (inputRequestSockets_[descr]->waitForBytesWritten())
        qDebug() << "get response sended";
    else
        throw SocketError(inputRequestSockets_[descr]->errorString().toStdString());

}
