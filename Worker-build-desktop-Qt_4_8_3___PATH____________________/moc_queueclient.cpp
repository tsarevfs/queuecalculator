/****************************************************************************
** Meta object code from reading C++ file 'queueclient.h'
**
** Created: Fri Apr 25 21:20:41 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Worker/queueclient.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'queueclient.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QueueClient[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      18,   13,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
      34,   12,   12,   12, 0x0a,
      50,   12,   12,   12, 0x0a,
      70,   12,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_QueueClient[] = {
    "QueueClient\0\0data\0get(QByteArray)\0"
    "queueResponse()\0queueDisconnected()\0"
    "udpReadReady()\0"
};

void QueueClient::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QueueClient *_t = static_cast<QueueClient *>(_o);
        switch (_id) {
        case 0: _t->get((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 1: _t->queueResponse(); break;
        case 2: _t->queueDisconnected(); break;
        case 3: _t->udpReadReady(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QueueClient::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QueueClient::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QueueClient,
      qt_meta_data_QueueClient, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QueueClient::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QueueClient::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QueueClient::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QueueClient))
        return static_cast<void*>(const_cast< QueueClient*>(this));
    return QObject::qt_metacast(_clname);
}

int QueueClient::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void QueueClient::get(QByteArray _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
