#include "queueclient.h"
#include "../MessageQueue/exceptions.h"
#include "../MessageQueue/requests.h"
#include "../MessageQueue/serialisation.h"


QueueClient::QueueClient(int udpPort, QObject *parent) :
    QObject(parent)
  , tcpSocket_(new QTcpSocket(this))
  , udpSocket_(new QUdpSocket(this))
  , serverAddress_("192.168.104.227")
  , serverTcpPort_(55555)
  , udpPort_(udpPort)
{
    connect(udpSocket_, SIGNAL(readyRead()), this, SLOT(udpReadReady()));
    udpSocket_->bind(udpPort_);

    connect(tcpSocket_, SIGNAL(readyRead()), this, SLOT(queueResponse()));
    connect(tcpSocket_, SIGNAL(disconnected()), this, SLOT(queueDisconnected()));
    tcpSocket_->connectToHost(serverAddress_, serverTcpPort_);
    if (tcpSocket_->waitForConnected(10000))
        qDebug() << "connected";
    else
        throw SocketError(tcpSocket_->errorString().toStdString());
}

void QueueClient::put(QString queueName, QByteArray data)
{
    QByteArray request;
    request.append((char)MQ_PUT);

    QByteArray rawQueueName = queueName.toUtf8();
    request.append(serializedInt(rawQueueName.length()));
    request.append(rawQueueName);

    request.append(serializedInt(data.size()));
    request.append(data);

    tcpSocket_->write(request);

    if (tcpSocket_->waitForBytesWritten())
        qDebug() << "put request sended";
    else
        throw SocketError(tcpSocket_->errorString().toStdString());
}

void QueueClient::subscribe(QString queueName)
{
    QByteArray request;
    request.append((char)MQ_SUBSCRIBE);

    request.append(serializedInt(udpPort_));

    QByteArray rawQueueName = queueName.toUtf8();
    request.append(serializedInt(rawQueueName.length()));
    request.append(rawQueueName);

    tcpSocket_->write(request);

    if (tcpSocket_->waitForBytesWritten())
        qDebug() << "subscribe request sended";
    else
        throw SocketError(tcpSocket_->errorString().toStdString());

    subscribtion_ = queueName;
}

void QueueClient::queueResponse()
{
    queueResponseBuffer_.append(tcpSocket_->readAll());

    while (queueResponseBuffer_.size() >= 1)
    {

        char success = queueResponseBuffer_[0];
        if (success == RS_DENIED)
        {
            queueResponseBuffer_ = queueResponseBuffer_.mid(1);
            continue;
        }

        int pos = 1;

        int dataLen = deserealizedInt(queueResponseBuffer_.mid(pos, sizeof(qint32)));
        pos += sizeof(qint32);

        if (queueResponseBuffer_.size() < pos + dataLen)
            return;

        QByteArray data = queueResponseBuffer_.mid(pos, dataLen);
        pos += dataLen;

        get(data);

        queueResponseBuffer_ = queueResponseBuffer_.mid(pos);
    }

}

void QueueClient::queueDisconnected()
{
    throw SocketError("disconnected from queue");
}

void QueueClient::udpReadReady()
{
    qDebug() << "notification recived";
    while (udpSocket_->hasPendingDatagrams())
    {
        QByteArray datagram;
        datagram.resize(udpSocket_->pendingDatagramSize());
        QHostAddress senderAddr;
        quint16 senderPort;
        udpSocket_->readDatagram(datagram.data(), datagram.size(), &senderAddr, &senderPort);
        QString queueName = QString::fromUtf8(datagram);
        if (queueName == subscribtion_)
            sendGetRequest(queueName);
    }
}

void QueueClient::sendGetRequest(QString queueName)
{
    QByteArray request;
    request.append((char)MQ_GET);

    QByteArray rawQueueName = queueName.toUtf8();
    request.append(serializedInt(rawQueueName.length()));
    request.append(rawQueueName);

    tcpSocket_->write(request);

    if (tcpSocket_->waitForBytesWritten())
        qDebug() << "get request sended";
    else
        throw SocketError(tcpSocket_->errorString().toStdString());
}
