#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QObject>
#include <QString>
#include <QStringList>
#include "queueclient.h"

class Calculator : public QObject
{
    Q_OBJECT
public:
    Calculator(QObject *parent = 0);

    QStringList operations() const;

    QString appendExpression(QString expression, QString symbol, bool isResult);
    void processExpression(QString expression);
    
signals:
    void resultReady(QString);
    
public slots:
    void resultFromQueue(QByteArray result);

private:
    QStringList operations_;
    QueueClient *queueClient_;
    
};

#endif // CALCULATOR_H
