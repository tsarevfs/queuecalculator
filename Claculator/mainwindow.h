#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSignalMapper>
#include "calculator.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
     void clicked(const QString & text);
     void showResult(QString result);

private:
     void clear();

     void addButton(QString text);
     void addButton(QString text, int row, int col);

    
private:
    Ui::MainWindow *ui;
    Calculator *calculator_;
    QSignalMapper *signalMapper_;
    bool showResult_;

};

#endif // MAINWINDOW_H
