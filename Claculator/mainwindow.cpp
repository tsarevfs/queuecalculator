#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QPushButton>
#include <QString>
#include <QStringList>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    calculator_(new Calculator(this)),
    signalMapper_(new QSignalMapper(this)),
    showResult_(false)
{
    ui->setupUi(this);
    window()->setFixedSize(size());

    for (int i = 0; i < 9; ++i)
    {
        addButton(QString::number(i), 2 - i / 3, i % 3);
    }
    addButton(".", 4, 0);

    QStringList commands;
    commands.append("C");
    commands.append("=");
    foreach (QString command, commands)
    {
        addButton(command);
    }

    foreach (QString operation, calculator_->operations())
    {
      addButton(operation);
    }

    connect(signalMapper_, SIGNAL(mapped(const QString &)), this, SLOT(clicked(const QString &)));

    connect(calculator_, SIGNAL(resultReady(QString)), this, SLOT(showResult(QString)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::clicked(const QString &text)
{
    if (text == "=")
    {
        calculator_->processExpression(ui->lineEdit->text());
        return;
    }
    if(text == "C")
    {
        clear();
        return;
    }

    QString currentExpr = ui->lineEdit->text();
    currentExpr = calculator_->appendExpression(currentExpr, text, showResult_);
    ui->lineEdit->setText(currentExpr);
    showResult_ = false;
}

void MainWindow::showResult(QString result)
{
    ui->lineEdit->setText(result);
    showResult_ = true;
}

void MainWindow::clear()
{
    ui->lineEdit->clear();
}

void MainWindow::addButton(QString text)
{
    QPushButton *button = new QPushButton(text);
    connect(button, SIGNAL(clicked()), signalMapper_, SLOT(map()));
    signalMapper_->setMapping(button, text);
    ui->verticalLayout->addWidget(button);
}

void MainWindow::addButton(QString text, int row, int col)
{
    QPushButton *button = new QPushButton(text);
    connect(button, SIGNAL(clicked()), signalMapper_, SLOT(map()));
    signalMapper_->setMapping(button, text);
    ui->gridLayout->addWidget(button, row, col);
}
