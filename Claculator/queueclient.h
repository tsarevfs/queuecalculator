#ifndef QUEUECLIENT_H
#define QUEUECLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QUdpSocket>
#include <QByteArray>


class QueueClient : public QObject
{
    Q_OBJECT
public:
    explicit QueueClient(int port, QObject *parent = 0);

    void put(QString queueName, QByteArray data);
    void subscribe(QString queueName);
    
signals:
    void get(QByteArray data);
    
public slots:
    void queueResponse();
    void queueDisconnected();
    void udpReadReady();

private:
    void sendGetRequest(QString queueName);

private:
    QTcpSocket *tcpSocket_;
    QUdpSocket *udpSocket_;
    QHostAddress serverAddress_;
    QString subscribtion_;

    QByteArray queueResponseBuffer_;
    int serverTcpPort_;
    int udpPort_;
};

#endif // QUEUECLIENT_H
