#-------------------------------------------------
#
# Project created by QtCreator 2014-04-25T13:32:25
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Claculator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    calculator.cpp \
    queueclient.cpp

HEADERS  += mainwindow.h \
    calculator.h \
    queueclient.h

FORMS    += mainwindow.ui
QMAKE_CXXFLAGS += -std=c++0x
