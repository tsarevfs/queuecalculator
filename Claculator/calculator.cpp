#include "calculator.h"

Calculator::Calculator(QObject *parent) :
    QObject(parent)
  , queueClient_(new QueueClient(55556, this))
{
    operations_.append("+");
    operations_.append("-");
    operations_.append("*");
    operations_.append("/");

    connect(queueClient_, SIGNAL(get(QByteArray)), this, SLOT(resultFromQueue(QByteArray)));
    queueClient_->subscribe("results");
}

QStringList Calculator::operations() const
{
    return operations_;
}

QString Calculator::appendExpression(QString expression, QString symbol, bool isResult)
{
    if (operations_.contains(symbol))
    {
        if (expression.size() == 0)
            return "";

        QString lastSymbol = expression.right(1);
        if (operations_.contains(lastSymbol))
            return expression.left(expression.length() - 1) + symbol;
        return expression + symbol;
    }

    if (symbol == "." && expression.size() != 0 && expression.right(1) == ".")
        return expression;

    if (isResult)
        return symbol;

    return expression + symbol;
}

void Calculator::processExpression(QString expression)
{
    queueClient_->put("works", expression.toUtf8());
}


void Calculator::resultFromQueue(QByteArray result)
{
    resultReady(QString::fromUtf8(result));
}
