#include "evaluator.h"

using namespace std;
#include <boost/lexical_cast.hpp>

namespace
{
bool IsSpace(char c)
{
    return isspace(c);
}

bool IsMathOperator(string c)
{
    return c == "+" ||
            c == "-" ||
            c == "*" ||
            c == "/";
}
}


Evaluator::Evaluator()
{
}

bool Evaluator::Rpn(const string expression, vector<string> &output)
{
    output.clear();
    char *end;
    vector<string> operator_stack;
    bool expecting_operator = false;

    for (const char *ptr = expression.c_str(); *ptr; ++ptr) {
        if (IsSpace(*ptr))
            continue;

        /* Is it a number? */
        if (!expecting_operator) {
            double number = strtod(ptr, &end);
            if (end != ptr) {
                /* Okay, it's a number */
                output.push_back(boost::lexical_cast<string>(number));
                ptr = end - 1;
                expecting_operator = true;
                continue;
            }
        }

        if (*ptr == '(') {
            operator_stack.push_back("(");
            expecting_operator = false;
            continue;
        }

        if (*ptr == ')') {
            while (operator_stack.size() && operator_stack.back() != "(") {
                output.push_back(operator_stack.back());
                operator_stack.pop_back();
            }

            if (!operator_stack.size())
                return false; /* Mismatched parenthesis */

            expecting_operator = true;
            operator_stack.pop_back(); /* Pop '(' */
            continue;
        }

        if (*ptr == '+' || *ptr == '-') {
            while (operator_stack.size() && IsMathOperator(operator_stack.back())) {
                output.push_back(operator_stack.back());
                operator_stack.pop_back();
            }

            operator_stack.push_back(boost::lexical_cast<string>(*ptr));
            expecting_operator = false;
            continue;
        }

        if (*ptr == '*' || *ptr == '/') {
            while (operator_stack.size() && (operator_stack.back() == "*" || operator_stack.back() == "/")) {
                output.push_back(operator_stack.back());
                operator_stack.pop_back();
            }

            operator_stack.push_back(boost::lexical_cast<string>(*ptr));
            expecting_operator = false;
            continue;
        }

        /* Error */
        return false;
    }

    while (operator_stack.size()) {
        if (!IsMathOperator(operator_stack.back()))
            return false;

        output.push_back(operator_stack.back());
        operator_stack.pop_back();
    }

    return true;
} // Rpn

/***************************************************************************************/

bool Evaluator::Calc(const string expression, double &output)
{
    vector<string> rpn;

    if (!Rpn(expression, rpn))
        return false;

    vector<double> tmp;
    for (size_t i = 0; i < rpn.size(); ++i) {
        if (IsMathOperator(rpn[i])) {
            if (tmp.size() < 2)
                return false;
            double two = tmp.back();
            tmp.pop_back();
            double one = tmp.back();
            tmp.pop_back();
            double result;

            switch (rpn[i][0]) {
                case '*':
                    result = one * two;
                    break;

                case '/':
                    result = one / two;
                    break;

                case '+':
                    result = one + two;
                    break;

                case '-':
                    result = one - two;
                    break;

                default:
                    return false;
            }

            tmp.push_back(result);
            continue;
        }

        tmp.push_back(atof(rpn[i].c_str()));
        continue;
    }

    if (tmp.size() != 1)
        return false;

    output = abs(tmp.back());
    return true;
} // Calc
