#include "worker.h"
#include "evaluator.h"

Worker::Worker(QObject *parent) :
    QObject(parent)
  , queueClient_(new QueueClient(55557, this))
{
    connect(queueClient_, SIGNAL(get(QByteArray)), this, SLOT(newWork(QByteArray)));
            queueClient_->subscribe("works");
}

void Worker::newWork(QByteArray data)
{
    Evaluator evaluator;
    QByteArray result;

    double output;
    if (evaluator.Calc(QString::fromUtf8(data).toStdString(), output))
        result = QString::number(output).toUtf8();
    else
        result = QString("error").toUtf8();

    queueClient_->put("results", result);
}
