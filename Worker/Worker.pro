#-------------------------------------------------
#
# Project created by QtCreator 2014-04-25T20:43:51
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = Worker
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    worker.cpp \
    queueclient.cpp \
    evaluator.cpp

HEADERS += \
    worker.h \
    queueclient.h \
    evaluator.h
QMAKE_CXXFLAGS += -std=c++0x
