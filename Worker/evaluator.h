#ifndef EVALUATOR_H
#define EVALUATOR_H
#include <vector>
#include <string>


class Evaluator
{
public:
    Evaluator();
    bool Calc(const std::string expression, double &output);

private:
    bool Rpn(const std::string expression, std::vector<std::string> &output);
};

#endif // EVALUATOR_H
