#ifndef WORKER_H
#define WORKER_H

#include <QObject>
#include "queueclient.h"

class Worker : public QObject
{
    Q_OBJECT
public:
    explicit Worker(QObject *parent = 0);
    
signals:
    
public slots:
    void newWork(QByteArray data);

private:
    QueueClient *queueClient_;
    
};

#endif // WORKER_H
