/****************************************************************************
** Meta object code from reading C++ file 'messagequeue.h'
**
** Created: Fri Apr 25 20:17:36 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../MessageQueue/messagequeue.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'messagequeue.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MessageQueue[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x0a,
      30,   13,   13,   13, 0x0a,
      48,   13,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MessageQueue[] = {
    "MessageQueue\0\0newConnection()\0"
    "clientReadReady()\0clientDisconnected()\0"
};

void MessageQueue::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MessageQueue *_t = static_cast<MessageQueue *>(_o);
        switch (_id) {
        case 0: _t->newConnection(); break;
        case 1: _t->clientReadReady(); break;
        case 2: _t->clientDisconnected(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData MessageQueue::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MessageQueue::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_MessageQueue,
      qt_meta_data_MessageQueue, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MessageQueue::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MessageQueue::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MessageQueue::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MessageQueue))
        return static_cast<void*>(const_cast< MessageQueue*>(this));
    return QObject::qt_metacast(_clname);
}

int MessageQueue::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
